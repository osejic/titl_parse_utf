#!/usr/bin/env python
# -*- coding: utf-8 -*-
import glob
import os
import tkinter as tk
from tkinter import filedialog
from pathlib import Path

root = tk.Tk()
root.withdraw()
file_path = filedialog.askopenfilename()

if ".srt" not in str(file_path):
	print("ERROR: nije prevod")
	exit()


home = str(Path.home())
home = home.replace("\\", "/")
home = home + "/Desktop/titl_uredjen.srt"
print (home)

file_titl = open(file_path, "r")
file_uredjen = open(home, "w", encoding="utf-8")


for ln in file_titl:
	
	if ("è" in  ln):
		ln = ln.replace("è","č")
		
	if ("æ" in ln):
		ln = ln.replace("æ","ć")
	
	if ("ð" in ln):
		ln = ln.replace("ð","đ")

	if ("È" in ln):
		ln = ln.replace("È","Č")
			
	if ("Æ" in ln):
		ln = ln.replace("Æ","Ć")		
		
	file_uredjen.write(ln) 




file_uredjen.close()
file_titl.close()